This is a modified version of: https://bitbucket.org/RopeRobotics/ur-interface/src/master/ from RopeRobotics, removing most of the additional deps, which are not particularly useful for us. Additionally it repackages everything so it can be installed via pip.

The Example.py show how to get started using this library

The purpose of this repository is to provide a framework that enables communication with a Universal Robots robot (UR3, UR5 or UR10).

The intention is to make the full script manual available, but there is still parts missing to be implemented.

The protocol implemented is:

URBasic:

Part of “Real Time Client” only the send program part.

RTDE for sending and receiving data

Dashboard server mainly for resting errors.