from setuptools import find_packages, setup

setup(
    name="ur-interface",
    version="1.2.6",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "math3d",
        "ikpy",
        "imutils",
        "pyserial",
        "bitstring"
    ],
)